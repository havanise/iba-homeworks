$(document).ready(function () {
$('.slick-bigger').slick({
        arrows: false,
        slidesToShow: 1,
        fade: true,
        asNavFor: '.slick-smaller'
    });
    $('.slick-smaller').slick({
        centerMode: true,
        centerPadding: '90px',
        slidesToShow: 3,
        asNavFor: '.slick-bigger'
    });



// ........our service tabs.....//
$('.tabs_title').click(function(){
        let  id=$(this).data('id');
        $('.tabs_title').removeClass('active');
        $(this).addClass('active');
        $('.tabcontent').removeClass('tab_content_active');
        $('#tab_content_'+id).addClass('tab_content_active');
    });



//...................our work..........//
const portfolio = ['img/graphicdesign/graphic-design1.jpg', 'img/graphicdesign/graphic-design2.jpg', 'img/graphicdesign/graphic-design3.jpg', 'img/graphicdesign/graphic-design4.jpg', 'img/graphicdesign/graphic-design5.jpg', 'img/graphicdesign/graphic-design6.jpg',
            'img/graphicdesign/graphic-design7.jpg', 'img/graphicdesign/graphic-design8.jpg', 'img/graphicdesign/graphic-design9.jpg', 'img/graphicdesign/graphic-design10.jpg', 'img/graphicdesign/graphic-design11.jpg','img/graphicdesign/graphic-design12.jpg',
  'img/webdesign/web-design1.jpg', 'img/webdesign/web-design2.jpg', 'img/webdesign/web-design3.jpg', 'img/webdesign/web-design4.jpg', 'img/webdesign/web-design5.jpg',  'img/webdesign/web-design6.jpg',    
'img/landingpage/landing-page1.jpg', 'img/landingpage/landing-page2.jpg', 'img/landingpage/landing-page3.jpg','img/landingpage/landing-page4.jpg','img/landingpage/landing-page5.jpg','img/landingpage/landing-page6.jpg',
'img/landingpage/landing-page7.jpg', 'img/wordpress/wordpress1.jpg', 'img/wordpress/wordpress2.jpg', 'img/wordpress/wordpress3.jpg','img/wordpress/wordpress4.jpg', 'img/wordpress/wordpress5.jpg','img/wordpress/wordpress6.jpg',
'img/wordpress/wordpress7.jpg', 'img/wordpress/wordpress8.jpg', 'img/wordpress/wordpress9.jpg',  'img/wordpress/wordpress10.jpg'];


$(".work_category").click(function () {
    let id=$(this).data('id');
        $(".work_category").removeClass("work_active");
        $(this).addClass("work_active");
        if (id === "graphic_design"){
            $(".work_image").remove();
            for (let i = 0; i < 12; i++){
                let image = `<div class="work_image"> 
                                        <img src="img/graphicdesign/graphic-design${(i+1)}.jpg" >
                                        <div class="description"> 
                                          <a href="" class="link_icon"><i class="fas fa-link "></i></a>
                                           <a href="" class="link_icon"><i class="fas fa-search "></i></a>   
                                            <h3 class="green_color">creative design</h3>
                                            <p class="image_paragraph">graphic design</p>
                                        </div>
                                    </div>`;
                $(".work-content").append(image);
            }
        }
        else if (id === "web design"){
            $(".work_image").remove();
            for (let i = 0; i < 7; i++){
                let image = `<div class="work_image"> 
                                        <img src="img/webdesign/web-design${(i+1)}.jpg" >
                                        <div class="description"> 
                                          <a href="" class="link_icon"><i class="fas fa-link "></i></a>
                                           <a href="" class="link_icon"><i class="fas fa-search "></i></a>   
                                            <h3 class="green_color">creative design</h3>
                                            <p class="image_paragraph">web design</p>
                                        </div>
                                    </div>`;
                $(".work-content").append(image);
            }
        }

        else if (id === "landing_pages"){
            $(".work_image").remove();
            for (let i = 0; i < 7; i++){
                 let image = `<div class="work_image"> 
                                        <img src="img/landingpage/landing-page${(i+1)}.jpg" >
                                        <div class="description"> 
                                          <a href="" class="link_icon"><i class="fas fa-link "></i></a>
                                           <a href="" class="link_icon"><i class="fas fa-search "></i></a>   
                                            <h3 class="green_color">creative design</h3>
                                            <p class="image_paragraph">web design</p>
                                        </div>
                                    </div>`;
                $(".work-content").append(image);
            }
        }

        else if (id === "wordpress"){
            $(".work_image").remove();
            for (let i = 0; i < 10; i++){
                 let image = `<div class="work_image"> 
                                        <img src="img/wordpress/wordpress${(i+1)}.jpg" >
                                        <div class="description"> 
                                          <a href="" class="link_icon"><i class="fas fa-link "></i></a>
                                           <a href="" class="link_icon"><i class="fas fa-search "></i></a>   
                                            <h3 class="green_color">creative design</h3>
                                            <p class="image_paragraph">web design</p>
                                        </div>
                                    </div>`;
                $(".work-content").append(image);
            }
        }

        else if (id === "all"){
            $(".image").remove();
            for (let i = 0; i < portfolio.length; i++){
                 let image = `<div class="work_image"> 
                                        <img src="${portfolio[i]}" >
                                        <div class="description"> 
                                          <a href="" class="link_icon"><i class="fas fa-link "></i></a>
                                           <a href="" class="link_icon"><i class="fas fa-search "></i></a>   
                                            <h3 class="green_color">creative design</h3>
                                            <p class="image_paragraph">all</p>
                                        </div>
                                    </div>`;
                $(".work-content").append(image);
            }
        }
    });

    let count = 12;
    $(".load_btn").click(function () {
        count +=12;
        if (count == 24){
            for (let i = 0; i < 12; i++){
               let image = `<div class="work_image"> 
                                        <img src="img/graphicdesign/graphic-design${(i+1)}.jpg" >
                                        <div class="description"> 
                                          <a href="" class="link_icon"><i class="fas fa-link "></i></a>
                                           <a href="" class="link_icon"><i class="fas fa-search "></i></a>   
                                            <h3 class="green_color">creative design</h3>
                                            <p class="image_paragraph">graphic design</p>
                                        </div>
                                    </div>`;
                $(".work-content").append(image);
            }$(".load_btn").hide();
        }

        else if (count == 36){
            for (let i = 0; i < 11; i++){
               let image = `<div class="work_image"> 
                                        <img src="${portfolio[i]}" >
                                        <div class="description"> 
                                          <a href="" class="link_icon"><i class="fas fa-link "></i></a>
                                           <a href="" class="link_icon"><i class="fas fa-search "></i></a>   
                                            <h3 class="green_color">creative design</h3>
                                            <p class="image_paragraph">all</p>
                                        </div>
                                    </div>`;
            }
            
        }
    });
});