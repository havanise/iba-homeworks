let inputFocus=document.getElementById('inputId');
let label = document.getElementById('label');
let inpField = document.getElementById('inputField');
const span = document.createElement('span');
const phrase= document.createElement('p');
inputFocus.onfocus=function(){
    inputFocus.style.border = "1px solid green";
    inputFocus.style.color = "green";
    span.remove();
}
inputFocus.addEventListener("focusout", myFunction);

function myFunction() {
	let inputVal = inputFocus.value;
	if (inputVal<0) {
		inputFocus.style.border = "1px solid red";
    
    phrase.innerText = "Please enter correct price";
    inpField.appendChild(phrase);

	}else{
    phrase.remove();
  	inputFocus.style.border = "1px solid black";
  	const button = document.createElement('button');
  	span.innerText = "Current price: $" + inputVal;
  	button.innerText = "X";
 	  label.before(span);
  	span.appendChild(button);
  	button.onclick= function(){
  	span.remove();
    inputFocus.value=0;
    
  }
  }
}

