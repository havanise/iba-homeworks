$(document).ready(function () {
    $('.menu_list_link').click(function(){
        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href' )).offset().top
        }, 1000);
    });

$('.upButton').click(function(){
        $('html, body').animate({
            scrollTop: 0
        }, 2000);
    });
    $(window).scroll(function() {
        if($(this).scrollTop() < 530) {
            $(".upButton").addClass("scroll")
        }
        else {
            $(".upButton").removeClass("scroll")
        }
    });
    let toggle_flag = false;
    $(".toggle").text("hide");
    $(".toggle").click(function () {
        if (toggle_flag == false){
            $(".toggle").text("show");
            toggle_flag = true;
        }
        else {
            $(".toggle").text("hide");
            toggle_flag = false;
        }
        $(".news_images").slideToggle("slow");
    })
});