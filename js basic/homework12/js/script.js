addEventListener("load",() => {
    let index = 0;
    const slides = document.querySelectorAll(".image-to-show");
    const classHide = "slides-hidden";
    const count = slides.length;
    nextSlide();
    function nextSlide() {
        let slideOne = slides[(index ++) % count].classList.add(classHide);
        let slideTwo = slides[index % count].classList.remove(classHide);   
        t = setTimeout(nextSlide, 10000);
    }
    let stopBtn = document.getElementById('stop')
	stopBtn.onclick = function(){
		clearTimeout(t);
		t = 0;
	}
	let startBtn = document.getElementById('start')
	startBtn.onclick = function(){
		t = setTimeout(nextSlide, 10000);
	}
});
