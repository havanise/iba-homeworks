let btns = document.getElementsByClassName("btn");

window.onkeydown = function (e) {
    for (let i = 0; i < btns.length; i++){
        btns[i].className = btns[i].className.replace(" blue-back", "");
    }
    if (e.code == "Enter"){
        btns[0].className += " blue-back";
    }
    else if (e.code == "KeyS"){
        btns[1].className += " blue-back";
    }
    else if (e.code == "KeyE"){
        btns[2].className += " blue-back";
    }
    else if (e.code == "KeyO"){
        btns[3].className += " blue-back";
    }
    else if (e.code == "KeyN"){
        btns[4].className += " blue-back";
    }
    else if (e.code == "KeyL"){
        btns[5].className += " blue-back";
    }
    else if (e.code == "KeyZ"){
        btns[6].className += " blue-back";
    }
};
